candidateApp.service('candidateService', function(){

  var self = this;

  this.candidates = [
    {
      name: 'Muhammad Ali Mansoor',
      isEmployed: false,
      jobId: 1,
      availability: new Date(2016, 5, 10),
      email: 'muhammad.mansoor@live.com',
      street: 'Beauchamp Street',
      state: 'VIC',
      desiredSalary: 80000,
      currentSalary: 65000
    },
    {
      name: 'John Doe',
      isEmployed: true,
      jobId: 2,
      availability: new Date(2016, 10, 10),
      email: 'john.doe@live.com',
      street: 'Bell Street',
      state: 'NSW',
      desiredSalary: 50000,
      currentSalary: 45000
    }

  ];



  this.getCandidate = function(jobId)
  {
    console.log(jobId);
  };

});
