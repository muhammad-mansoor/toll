candidateApp.config( function($routeProvider) {

  $routeProvider

  .when('/', {
    templateUrl: 'views/home.html',
    controller: 'HomeController'
  })

  .when('/profile/:jobId', {
    templateUrl: 'views/profile.html',
    controller: 'ProfileController'
  })

  .otherwise({
  		redirectTo: '/'
  })


});
