Toll Developer Test
===================

Developer
---------

The project is built using AngularJS and Twitter Bootstrap frameworks.

Run `bower install` and `npm install` to install developer dependencies. 

### git
Use `master` for releases, and tag their versions. `develop` is for branching out `features`.
